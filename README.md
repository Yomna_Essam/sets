class Set that resembles the Set concept in mathematics.
set can store only one copy of each value.
No duplicates are allowed.
Items are stored in order.
Items cannot be accessed by index.
set expands if it reaches the maximum capacity.
check if set is empty or no.
insert new element in set.
remove element from set.
search for element in set.
check if set is subset from another set or no .
clear the set.
